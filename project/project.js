'use strict';

angular.module('myApp.project', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/project1', {
    templateUrl: 'project/display.html',
    controller: 'ProjectCtrl'
  });
}])
.controller('ProjectCtrl', ['$scope', '$http', '$localStorage', '$window', 'ConfigOptions', '$route', '$mdDialog', 'SubdomainService',  function($scope, $http, $localStorage, $window, ConfigOptions, $route, $mdDialog, SubdomainService) {




$scope.showDialog = function(saveFunc) {
    $mdDialog.show({
                  clickOutsideToClose: true,
                  scope: $scope,        
                  preserveScope: true,     
                  templateUrl: $scope.selectedTemplate,      
                  controller: function DialogController($scope, $mdDialog, $route) {
                     $scope.closeDialog = function() {
                        $mdDialog.hide();
                     }
                     $scope.saveDialog = function() {                      
                      saveFunc();                      
                      $scope.closeDialog();
                     }
                  }
               });
  }  




}]);
