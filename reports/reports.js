'use strict';

angular.module('myApp.reports', ['ngRoute','ngMaterial','ngMessages'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/reports', {
    templateUrl: 'reports/reports.html',
    controller: 'ReportsCtrl'
  });
}])
.directive('reportfilter', function() {
    return {
        templateUrl: 'reports/reportfilter.html',
        controller: 'ReportsCtrl'
    }
})
.controller('ReportsCtrl', ['$scope', '$http', '$localStorage', '$window', 'userValidation', 'ConfigOptions', '$mdBottomSheet', '$mdToast', 'SubdomainService', '$route', 'navService', 'dataService', '$mdDialog', function($scope, $http, $localStorage, $window, userValidation, ConfigOptions, $mdBottomSheet, $mdToast, SubdomainService, $route, navService, dataService, fileInput, $mdDialog) {
	   $scope.user = $localStorage.user;
     $scope.selected = [];

     $scope.numTypes = ['Integer', 'Float', 'Currency'];

      $scope.toggle = function (item, list) {
        var idx = list.indexOf(item);
        if (idx > -1) {
          list.splice(idx, 1);
        }
        else {
          list.push(item);
        }
      };
      $scope.exists = function (item, list) {
        return list.indexOf(item) > -1;
      };
      $scope.inSelected = function(item) {
        return $scope.exists(item, $scope.selected);
      }

     $scope.runReport = function() {
        var data = {token:$localStorage.token, personid: $localStorage.personid, client:SubdomainService.company}; 
        $scope.isLoading = true;
        dataService.sendData(data, "/report/run", loadReport);
      
      }

      var loadReport = function(response) {
        console.log(response);
        $localStorage.chartResponse = response;
        populateReport();
        populateChart();
      }

      var populateReport = function() {
        $scope.reportTitle = $localStorage.chartResponse.data.title;
        $scope.reportData = $localStorage.chartResponse.data.rows;
        $scope.fields = $localStorage.chartResponse.data.fields;
        $scope.keys = $scope.reportData[0];
        // $scope.selected = $scope.keys;
        var i = 0;
        for(var k in $scope.keys) {
          $scope.selected[i] = k;
          i++;
        }


        // $scope.searchTerm = '';
        // $scope.tempSearchTerm = '';
        console.log($scope.reportData);

        $scope.isLoading = false;
      }
      var populateChart = function() {
        $scope.labels = $localStorage.chartResponse.data.labels;
        $scope.data = $localStorage.chartResponse.data.values;

        for(var i = 0; i < $scope.data.length; i++) {
          $scope.data[i] = Number($scope.data[i].replace(/[^0-9\.-]+/g,""));
        }
      }
      $scope.onClick = function (points, evt) {        
        
        var row = $scope.reportData[points[0]._index];
        console.log(row); //Selected report row
        $scope.searchTerm = points[0]._model.label;
        $scope.tempSearchTerm = points[0]._model.label;
        
        $scope.$apply();
        
      };
      
      $scope.showFilter = function() {
        $mdDialog.show({
          controller: ReportsCtrl,
          templateUrl: 'reportfilter.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true
      });
      }
      $scope.tableFilter = function(item) {
        var val = false;
        
        // console.log(item);
        for (var k in item){
            if (item.hasOwnProperty(k) && angular.isDefined($scope.searchTerm)) {
                 // console.log("Key is " + k + ", value is" + item[k]);
                if(item[k].toUpperCase().indexOf($scope.searchTerm.toUpperCase()) >= 0) {
                  // val = true;                  
                  for(var i = 0; i < $scope.fields.length; i++) {                      


                    if(k == $scope.fields[i].field && $scope.numTypes.indexOf($scope.fields[i].type) > -1 && item[k] >= $scope.fields[i].min && item[k] <= $scope.fields[i].max) {
                      console.log("Key is " + k + ", value is" + item[k]);
                      val = true;                        
                    } else if(k == $scope.fields[i].field && $scope.numTypes.indexOf($scope.fields[i].type) == -1) {
                      console.log("Key is " + k + ", value is" + item[k]);
                      val = true;
                    }
                  }
                }
                 
            }
          }

        // }
        
        return val;
      }
      $scope.fieldSlider = function(item) {
        console.log(item);
      }
      $scope.sliderchange = function(val) {
        console.log(val);
      }
      $scope.search = function() {
        $scope.searchTerm = $scope.tempSearchTerm;
        // $scope.$apply();
      }
      var checkResponse = function(response) {
        console.log(response);
        $scope.isLoading = false;
      }


      $scope.initReport = function() {
        // $scope.searchTerm = null;
        // $scope.tempSearchTerm = '';
        $scope.runReport();
      }

     }]);