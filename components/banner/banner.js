function BannerController($scope, $element, $attrs, $window, $localStorage, $mdSidenav, navService) {
  var ctrl = this;

  $scope.showHome = false;
  $scope.showAddTime = $localStorage.showAddTime;
  $scope.showAddExp = $localStorage.showAddExp;

  $scope.home = function() {
  	$localStorage.workid = -1;
  	$localStorage.expenseid = -1;
  	$window.location.href = '#!/home';
  }

  $scope.new = function() {
    navService.navigateTo('#!/timesheet/add');
  }
  $scope.newExp = function() {
      $localStorage.expenseid = -1;
      navService.navigateTo('#!/expense/create');   
  }
  // This would be loaded by $http etc.
  $scope.toggleLeft = buildToggler('left');
    
  function buildToggler(componentId) {
    return function() {
      $mdSidenav(componentId).toggle();
    };
  }  
}

angular.module('myApp').component('banner', {
  templateUrl: 'components/banner/banner.html',
  controller: BannerController
});