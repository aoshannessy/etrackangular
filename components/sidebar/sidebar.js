function SidebarController($scope, $element, $attrs, $localStorage, $window, $mdSidenav, navService) {
  var ctrl = this;

  $scope.user = $localStorage.user;
  $scope.name = $localStorage.fullName;
  console.log($scope.name);

  $scope.logout = function() {
  	$localStorage.$reset();
  	$window.location.href = '#!/logout';  

  }

  $scope.myexpenses = function() {
    $window.location.href = '#!/expense/user';   
    $mdSidenav('left').close();
  }

  $scope.timesheet = function() {
    navService.navigateTo('#!/timesheet');   
    $mdSidenav('left').close();
  }

  $scope.haswork  = function() {
    $window.location.href = '#!/home'; 
    $mdSidenav('left').close();
  }
  $scope.search  = function() {
    $window.location.href = '#!/search'; 
    $mdSidenav('left').close();
  }
  $scope.contacts = function() {
    $window.location.href = '#!/contact/list'; 
    $mdSidenav('left').close();    
  }
  $scope.close = function() {
    $mdSidenav('left').close();
  }
  // This would be loaded by $http etc.
}

angular.module('myApp').component('sidebar', {
  templateUrl: 'components/sidebar/mdsidebar.html',
  controller: SidebarController
});