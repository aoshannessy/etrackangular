function BackbuttonController($scope, $element, $attrs, $window, $localStorage, $mdSidenav) {
  var ctrl = this;
  console.log('Nav');

  // $localStorage.previousTitles = ["Home"];
  // $localStorage.previousURLs = ["#!/home"];

  $scope.previousTitle = $localStorage.previousTitles[$localStorage.previousTitles.length - 1];

  $scope.backButton = function() {
    console.log($localStorage.previousTitles);
    console.log($localStorage.previousURLs);
    console.log($localStorage.previousTEIDs);
    

    var pt = $localStorage.previousTitles[$localStorage.previousTitles.length - 1];
    var url = $localStorage.previousURLs[$localStorage.previousURLs.length - 1];
    var teid = -1;
    if($localStorage.previousTEIDs.length > 1) {
      teid = $localStorage.previousTEIDs[$localStorage.previousTEIDs.length - 2];
    }

    $localStorage.previousTitles.pop();
    $localStorage.previousURLs.pop();
    $localStorage.previousTEIDs.pop();


    $localStorage.workFull = pt; 
    $localStorage.teid = teid;
    $scope.pageTitle = pt;    

    $window.location.href = url;

    }

}

angular.module('myApp').component('backbutton', {
  templateUrl: 'components/backbutton/backbutton.html',
  controller: BackbuttonController
})
.service('navService', function($localStorage, $window) {

    this.navigateTo = function(destination) { 
      $localStorage.previousURLs = ( typeof $localStorage.previousURLs != 'undefined' && $localStorage.previousURLs instanceof Array ) ? $localStorage.previousURLs : [];
      $localStorage.previousTitles = ( typeof $localStorage.previousTitles != 'undefined' && $localStorage.previousTitles instanceof Array ) ? $localStorage.previousTitles : [];      
      $localStorage.previousTEIDs = ( typeof $localStorage.previousTEIDs != 'undefined' && $localStorage.previousTEIDs instanceof Array ) ? $localStorage.previousTEIDs : [];      
      $localStorage.previousTitles.push($localStorage.pageTitle);
      $localStorage.previousURLs.push($window.location.href);  
      if(angular.isDefined($localStorage.teid)) {
        $localStorage.previousTEIDs.push($localStorage.teid);
      } else {
        $localStorage.previousTEIDs.push('-1');
      }   
      
      $window.location.href = destination;
    }
    this.navigateToandFrom = function(destination, previous) { 
      $localStorage.previousURLs = ( typeof $localStorage.previousURLs != 'undefined' && $localStorage.previousURLs instanceof Array ) ? $localStorage.previousURLs : [];
      //$localStorage.previousTitles.push($scope.pageTitle);
      $localStorage.previousURLs.push(previous);    
      console.log($localStorage.previousURLs);
      $window.location.href = destination;
    }

    this.navigateBack = function() {
      console.log($localStorage.previousURLs);
      $window.location.href = $localStorage.previousURLs[$localStorage.previousURLs.length - 1];
      $localStorage.previousURLs.pop();      

    }
});