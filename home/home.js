'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'home/home.html',
    controller: 'HomeCtrl'
  });
  $routeProvider.when('/search', {
    templateUrl: 'home/search.html',
    controller: 'HomeCtrl'
  });
  $routeProvider.when('/display', {
    templateUrl: 'home/display.html',
    controller: 'HomeCtrl'
  });
}])

.controller('HomeCtrl', ['$scope', '$http', '$localStorage', '$window', 'ConfigOptions', 'SubdomainService', 'navService', function($scope, $http, $localStorage, $window, ConfigOptions, SubdomainService, navService) {
	$scope.initials = $localStorage.initials;
  $scope.name = $localStorage.fullName;

	var DisplayData = function() {

        $scope.isLoading = true;
        
  	    var data = { personid: $localStorage.personid, token:$localStorage.token, client:SubdomainService.company };
        $http({
      				method: 'POST',
  	    			url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/user/work", 
  	    			params : data
          	})
  		  .then(function(response) {  
          console.log(response);
  		    $scope.work = response.data.work;
          $localStorage.user.hasWork = $scope.work;
          $scope.isLoading = false;
 		    });
    }
    var DisplaySearch = function() {
        $scope.hideCombo = true;
        var data = { personid: $localStorage.personid, token:$localStorage.token, client:SubdomainService.company };
        $http({
              method: 'POST',
              url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/retrievealias", 
              params : data
            })
        .then(function(response) {  
         // $scope.aliasList  = response.data.aliases;
         $scope.aliasList = [];
          for(var i = 0; i<response.data.aliases.length; i++) {
            $scope.aliasList[i] = response.data.aliases[i].name; 
            
          }
          $scope.isLoading = false;
        });

      $scope.hideCombo = false;  
      // $scope.aliasList = ['Project', 'Contact'];
      //$scope.selectedAliasList = ['Project'] //, 'Task', 'Admin Task'];
    }

    var DisplayItem = function() {
      $scope.workFull = $localStorage.workFull;
      var data = { personid: $localStorage.personid, token:$localStorage.token, teid:$localStorage.teid, client:SubdomainService.company };
        $http({
              method: 'POST',
              url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/displayitem", 
              params : data
            })
        .then(function(response) {  
          console.log(response.data);
          $scope.items = response.data.items;
        });      
    }
    
    $scope.showWork = function(workid, name) {      
      showWorks(workid, name);
    }
    $scope.showWorkFromSearch = function(workid, name) {
      $scope.pageTitle = $scope.workFull;
      $localStorage.pageTitle = $scope.pageTitle;
      //$localStorage.previous = '#!/display';
      //$localStorage.previousTitle = $scope.workFull;
      //$scope.pageTitle = 'Search';
      showWorks(workid, name);
    }
    var showWorks = function(workid, name) {
      console.log(name);
      $scope.isLoading = true;
      $localStorage.teid = workid;
      $localStorage.workid = workid;
      $localStorage.workFull = name;      

      navService.navigateTo('#!/project');
      
      $scope.isLoading = false;
    }
    $scope.resultSelect = function(teid, name, alias) {
      console.log(teid);
      console.log(name);
      $localStorage.teid = teid;
      $localStorage.workFull = name; 
      console.log(alias);
      $localStorage.contactid = teid;
      switch (alias) {
        case 'contact':          
          navService.navigateTo('#!/display/contact'); 
          break;
        case 'Company or Couple':          
          navService.navigateTo('#!/display/contact'); 
          break;
        case 'Company':          
          navService.navigateTo('#!/display/contact'); 
          break;          
        case 'Individual':
          navService.navigateTo('#!/display/contact'); 
          break;
        default:          
          navService.navigateTo('#!/display'); 
          break;
      }
    }
    $scope.search = function() {

      if(angular.isUndefined($scope.searchtext)) {
        $scope.searchtext = '';  
      }
      if(angular.isUndefined($scope.sequencenumber)) {
        $scope.sequencenumber = '';
      }
      if($scope.searchtext.length >= 3 || $scope.sequencenumber.length >= 3) {        

        if($scope.selectedAliasList == null) {
          $scope.selectedAliasList = ['Project'];
        } 
        var aliasString = $scope.selectedAliasList.toString();        
        var data = { personid: $localStorage.personid, token:$localStorage.token, searchtext:$scope.searchtext, sequence:$scope.sequencenumber, alias:aliasString, client:SubdomainService.company };  
        $scope.isLoading = true;          
        
        $http({
          method: 'POST',
          url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/search", 
          params : data
        })
        .then(function(response) {  
          $scope.results = response.data.results;
          console.log($scope.results);
          $scope.message = response.data.message;  
          if(angular.isDefined($scope.message)) {
            $scope.messageExists = true;
          }    
          $scope.isLoading = false;
        });    
      } else {
        alert('One of Number or Name must include at least 3 characters.')
      }
    }

    var navigateTo = function(destination) {
      $localStorage.previousTitles.push($scope.pageTitle);
      $localStorage.previousURLs.push($window.location.href);

      console.log($localStorage.previousTitles);
      console.log($localStorage.previousURLs);

      $window.location.href = destination;
    }

    $scope.init = function () {      
      $scope.pageTitle = 'My Work';
      $localStorage.pageTitle = $scope.pageTitle;
      $localStorage.showAddTime = false;
      $localStorage.showAddExp = false;
    	DisplayData();
    }

    $scope.initSearch = function () {
      $scope.pageTitle = 'Search';  
      $localStorage.pageTitle = $scope.pageTitle;
      $scope.selectedAliasList = ["Project"];
      $localStorage.showAddTime = false;
      $localStorage.showAddExp = false;
      DisplaySearch();
    } 
    $scope.initDisplay = function () {
      $localStorage.showAddTime = false;
      $localStorage.showAddExp = false;
      $scope.pageTitle = $scope.workFull;
      $localStorage.pageTitle = $scope.pageTitle;
      DisplayItem();
    }   

}]);