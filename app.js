'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
  	'ngRoute',
  	'ngStorage',
    'ngSanitize',
  	'snap',  	
	  'myApp.login',
  	'myApp.home',
  	'myApp.expense',
    'myApp.contact',
    'myApp.project',
    'myApp.trackable',
    'myApp.timesheet',
    'myApp.reports',
    'angular.filter',
    'chart.js',
    'rzModule',
    
    // 'myApp.banner',
    // 'myApp.sidebar',
  	'myApp.version'
]).
config(['$locationProvider', '$routeProvider', 'snapRemoteProvider', '$mdDateLocaleProvider', function($locationProvider, $routeProvider, snapRemoteProvider, $mdDateLocaleProvider) {
  $locationProvider.hashPrefix('!');

  snapRemoteProvider.globalOptions.disable = 'left';
  snapRemoteProvider.globalOptions.touchToDrag = false;


  

  $mdDateLocaleProvider.formatDate = function(date) {
       return moment(date).format('ddd DD/MM/YYYY');

  };

  $routeProvider.otherwise({redirectTo: '/login'});
}]).
service('userValidation', function($localStorage, $window) {
    this.isValid = function(token) {
      console.log('Checking token');
      console.log(token);
      if (token=="Invalid token" || token == null) {
        $localStorage.$reset();
        $window.location.href = '#!/';
        console.log('Failed validation');
      }
    }
})
.service('dataService', function($http, ConfigOptions, userValidation) {
  this.sendData = function(data, url, callback) {
    console.log(data);
  $http({
            method: 'POST',
            url:ConfigOptions.hostname +":" + ConfigOptions.goport + url, 
            params : data
            })
        .then(function(response) { 
            userValidation.isValid(response.data.token); 
            callback(response);            
      });
  }
  this.sendSimple = function(data, callback) {
    console.log(data);    
    $http({
            method: 'POST',
            url:ConfigOptions.hostname +":" + ConfigOptions.goport + '/message/receive', 
            params : data
            })
        .then(function(response) { 
            userValidation.isValid(response.data.token); 
            callback(response);            
      });  
  }

})
.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                var f = reader.readAsDataURL(changeEvent.target.files[0]);
                console.log(f);
            });
        }
    }
}])
.factory('ConfigOptions', function() {
  return {
      client : 'archdemo',
      remotepath : 'programs',
      goport : "8080",
      hostname: "http://localhost"
      // hostname: "http://jupiter.spiderit.com"
  };
})
.factory('SubdomainService', ['$location', 'ConfigOptions', function($location, ConfigOptions) {
    var service = {};
    var host = $location.host();
    console.log(host);

    if (host.indexOf('.') >= 0) {
      service.company = host.split('.')[0];
    } else {
      service.company = ConfigOptions.client;
    }
    return service;
  }
]);
