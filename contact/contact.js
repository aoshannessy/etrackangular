'use strict';

angular.module('myApp.contact', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/display/contact', {
    templateUrl: 'contact/display.html',
    controller: 'ContactCtrl'
  });
  $routeProvider.when('/contact/list', {
    templateUrl: 'contact/list.html',
    controller: 'ContactCtrl'
  });
}])
.controller('ContactCtrl', ['$scope', '$http', '$localStorage', '$window', 'ConfigOptions', '$route', '$mdDialog', 'SubdomainService', 'navService', 'dataService', function($scope, $http, $localStorage, $window, ConfigOptions, $route, $mdDialog, SubdomainService, navService, dataService) {


$scope.initDisplayContact = function() {
  $localStorage.showAddTime = false;
  $localStorage.showAddExp = false;
  $scope.isLoading = true;
  var data = { itemTEID: $localStorage.contactid,personid:$localStorage.personid, token:$localStorage.token, client:SubdomainService.company, remotepath:ConfigOptions.remotepath };
  dataService.sendSimple(data, finishDetails);

}

$scope.initListContacts = function() {
  $localStorage.showAddTime = false;
  $localStorage.showAddExp = false;
  $scope.pageTitle = 'Contacts';
  $localStorage.pageTitle = $scope.pageTitle;
  $scope.isLoading = true;
  var data = { personid:$localStorage.personid, token:$localStorage.token, client:SubdomainService.company, remotepath:ConfigOptions.remotepath, maxID:'0', func:"displayContactList" };  
  dataService.sendSimple(data, finishList);  
}

$scope.contactSelect = function(teid, name, alias) {
      $localStorage.teid = teid;
      $localStorage.workFull = name; 
      $localStorage.contactid = teid; 
      navService.navigateTo('#!/display/contact'); 
    }

  var finishDetails = function(response) {
    $scope.contact = response.data;
    $scope.isLoading = false;
  }
  // var finishProgress = function(response) {
  //   $scope.contact.progress = response.data.progress;
  //   $scope.isLoading = false;
  // }
  var finishChildren = function(response) {
    $scope.contact.contacts = response.data.contacts;
    $scope.isLoading = false;
  }

  var tabs = ['Details', 'Progress Notes', 'Children'];
  $scope.$watch('selectedIndex', function(current, old){
    console.log('Current Tab: ' + tabs[current]);
    var cTab = tabs[current];

    // if(cTab == 'Progress Notes' && !angular.isDefined($scope.contact.progress)) {
    //   $scope.isLoading = true;
    //   var data = { itemTEID: $localStorage.contactid,personid:$localStorage.personid, token:$localStorage.token, client:SubdomainService.company, remotepath:ConfigOptions.remotepath };
    //   sendData(data, "/contact/display/progress", finishProgress);
    // } else 
    if(cTab == 'Children' && !angular.isDefined($scope.contact.contacts)) {
      $scope.isLoading = true;
      var data = { itemTEID: $localStorage.contactid,personid:$localStorage.personid, token:$localStorage.token, client:SubdomainService.company, remotepath:ConfigOptions.remotepath, func:"displayContactChildren" };
      dataService.sendSimple(data, finishChildren);
    } 
    
  });

  var finishList = function(response) {
    console.log(response);
    if(angular.isDefined($scope.contacts)) {
      $scope.contacts.push(response.data.contacts);

    } else {
      $scope.contacts = response.data.contacts; 
    }
    if(response.data.maxID !== 'complete') {
      var data = { personid:$localStorage.personid, token:$localStorage.token, client:SubdomainService.company, remotepath:ConfigOptions.remotepath, maxID:response.data.maxID, func:"displayContactList"};  
      dataService.sendSimple(data, finishList); 
    }

  }

$scope.childContact = function(teid) {
  $localStorage.contactid = teid;
  console.log("Loading child");
  $route.reload();
}

$scope.addProgress = function() {
  $scope.selectedTemplate = 'contact/dialogs/progressnote.html';
  $scope.showDialog(saveProgressNote);

}


$scope.showDialog = function(saveFunc) {
    $mdDialog.show({
                  clickOutsideToClose: true,
                  scope: $scope,        
                  preserveScope: true,     
                  templateUrl: $scope.selectedTemplate,      
                  controller: function DialogController($scope, $mdDialog, $route) {
                     $scope.closeDialog = function() {
                        $mdDialog.hide();
                     }
                     $scope.saveDialog = function() {                      
                      saveFunc();                      
                      $scope.closeDialog();
                     }
                  }
               });
  }  

var finishProgressAdd = function(response) {

}

var saveProgressNote = function() {
  console.log($scope.progressnote.text);
  var newNote = {
    date:moment().format('DD/MM/YYYY h:mm:ss A'),
    person:$localStorage.fullName,
    comment:$scope.progressnote.text
  }
  $scope.contact.progress.push(newNote);

  var data = { itemTEID: $localStorage.contactid,personid:$localStorage.personid, token:$localStorage.token, client:SubdomainService.company, remotepath:ConfigOptions.remotepath, note:$scope.progressnote.text, func:"progressAdd" };
  dataService.sendSimple(data, finishProgressAdd);
  $scope.progressnote.text = '';
}

}]);
