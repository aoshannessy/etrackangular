'use strict';

angular.module('myApp.timesheet', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/timesheet', {
    templateUrl: 'timesheet/timesheet.html',
    controller: 'TimesheetCtrl'
  });
  $routeProvider.when('/timesheet/add', {
    templateUrl: 'timesheet/add.html',
    controller: 'TimesheetCtrl'
  });  
  $routeProvider.when('/timesheet/edit', {
    templateUrl: 'timesheet/edit.html',
    controller: 'TimesheetCtrl'
  });    
}])
.directive('timesheetcell', function() {
    return {
        templateUrl: 'timesheet/cell.html',
        controller: 'TimesheetCtrl',
        bindings: {
           app: '<'
         }
    }
})
.directive('projectcell', function() {
    return {
        templateUrl: 'timesheet/projectcell.html',
        controller: 'TimesheetCtrl',
        bindings: {
           app: '<'
         }
    }
})
.controller('TimesheetCtrl', ['$scope', '$http', '$localStorage', '$window', 'ConfigOptions', '$route', '$mdDialog', 'SubdomainService', 'navService', 'dataService', '$mdToast',  function($scope, $http, $localStorage, $window, ConfigOptions, $route, $mdDialog, SubdomainService, navService, dataService, $mdToast) {


    //var data = {func: 'displayts',User: 'dir',password: 'e'};
	    console.log('UserID: ' + $localStorage.personid);
      $scope.snapOpts = {
        disable: 'right'
      };

      var DisplayData = function() {
        
        var data = {  date:$localStorage.delphiDate,
                      personid:$localStorage.personid, 
                      token:$localStorage.token, 
                      client:SubdomainService.company, 
                      remotepath:ConfigOptions.remotepath, 
                      userid: $localStorage.personid };
      	//console.log('5- ' + data);
        console.log(data);
        
        dataService.sendData(data, "/timesheet", finishDisplay)
      }
      var finishDisplay = function(response) {
        console.log(response);
        if(response.data != ']')
        {
          $scope.timesheet = response.data;
          $localStorage.timesheet = $scope.timesheet;
          $scope.user = $localStorage.user;
          colour();
        } else
        {
          $scope.timesheet = [{"work":"No records for these dates"}];
        }
        console.log($scope.timesheet);
        $scope.isLoading = false;
        console.log($scope.timesheet.SelectedStatus);

      }
      $scope.logout = function() {
        
        $localStorage.$reset();
        $window.location.href = 'login.html';
      }

      $scope.changeDate = function() {
        console.log('Changing date');
        var lDate = $scope.timesheetdate.value; 
        console.log('Date before format' + lDate);
        lDate = moment(lDate).startOf('isoweek').format('YYYYMMDD');
        updateDates(lDate);

        DisplayData();

      }
		  $scope.back = function() {
        var lDate = moment($localStorage.FullDate).subtract('1', 'weeks').startOf('isoweek').format('YYYYMMDD');
        console.log('1- ' + lDate);
        updateDates(lDate);

        DisplayData();
      }
      $scope.forwards = function() {
        var lDate = moment($localStorage.FullDate).add('1', 'weeks').startOf('isoweek').format('YYYYMMDD');
        console.log('1- ' + lDate);
        updateDates(lDate);

        DisplayData();
      }
      $scope.totalForDay = function(day) {
        //console.log($scope.timesheet);
        var total = 0;
          for(var i = 0; i < $scope.timesheet.tscells.length; i++){
            var cell = $scope.timesheet.tscells[i];
            if(cell.order == day) {
              total = total + parseInt(cell.effort);
            }
          //    var product = $scope.cart.products[i];
          //    total += (product.price * product.quantity);
          }
          
        total = total / 3600;
        return total;
      }
      $scope.totalForProject = function(project) {
        var total = 0;
          for(var i = 0; i < $scope.timesheet.tscells.length; i++){
            var cell = $scope.timesheet.tscells[i];
            if(cell.project == project) {
              total = total + parseInt(cell.effort);
            }
          }
          
        total = total / 3600;
        return total;
      }

      var colour = function(){
        var colour = 0;
        var colours = [];
        var prLength = 0;
        var extra = 0;
          for(var i = 0; i < $scope.timesheet.length; i++)
          {
              var cell = $scope.timesheet[i];
              prLength = cell.projectTEID % 5;
              if(colours.hasOwnProperty(prLength)) { //Colour has already been set to a project

                if(colours[prLength] == cell.project) { //Project has already been given a colour
                  $scope.timesheet[i].colour = prLength;  

                }
                console.log("Colour exists at " + prLength);
              } else {  //Set a new colour
                $scope.timesheet[i].colour = prLength;
                colours[prLength] = cell.project;
                console.log("New Colour at " + prLength);
              }
          }

      }        

      var updateDates = function(selectedDate)
      {
        console.log('2- Selected Date ' + selectedDate);
        var currentDate = moment(selectedDate).startOf('isoweek').format('MM/DD/YYYY');   
        console.log('3- Selected Date MM/DD ' + currentDate);    
        $scope.timesheetdate = {value: new Date(currentDate)};
        $localStorage.tsDate = currentDate;
        
        var delDate = moment(selectedDate).startOf('isoweek').format('DD/MM/YYYY'); 
        $localStorage.delphiDate = delDate;
        $localStorage.FullDate = selectedDate;



        $scope.forward = moment(selectedDate).add('1', 'weeks').startOf('isoweek').format('DD/MM/YYYY');
        $scope.backward = moment(selectedDate).subtract('1', 'weeks').startOf('isoweek').format('DD/MM/YYYY'); 
        console.log('4- '+ $scope.backward);
      }

      $scope.updateTsStatus = function() {
        
        if($scope.timesheet.SelectedStatus!==$scope.timesheet.tsStatus) {
          console.log($scope.timesheet);
          console.log($scope.timesheet.SelectedStatus);
          var data = {  
                      date:$localStorage.delphiDate,                      
                      personid:$localStorage.personid, 
                      token:$localStorage.token, 
                      client:SubdomainService.company, 
                      remotepath:ConfigOptions.remotepath, 
                      userid: $localStorage.personid,
                      SelectedStatus:$scope.timesheet.SelectedStatus
                    }

          dataService.sendData(data, '/timesheet/status/update', logResponse);
        }
      }
      $scope.lockTS = function() {
        $scope.isLoading = true;
        var data = {  
            date:$localStorage.delphiDate,                      
            personid:$localStorage.personid, 
            token:$localStorage.token, 
            client:SubdomainService.company, 
            remotepath:ConfigOptions.remotepath, 
            userid: $localStorage.personid            
          }

          dataService.sendData(data, '/timesheet/lock', reloadOnResponse);
      }
      var reloadOnResponse = function(response) {
        $route.reload();
      }
      $scope.selectWork = function() {        
        $scope.loadActivities();
        $scope.updateUnits();
        
      }
      $scope.selectBookTo = function() {
        console.log($scope.tscell);
        $scope.updateUnits();
      }
      $scope.loadActivities = function() {

        console.log($scope.tscell.work);

        var data = {  date:$localStorage.delphiDate,
                      workid:$scope.tscell.work,
                      personid:$localStorage.personid, 
                      token:$localStorage.token, 
                      client:SubdomainService.company, 
                      remotepath:ConfigOptions.remotepath, 
                      userid: $localStorage.personid
                    }

        dataService.sendData(data, '/timesheet/activities', placeActivites);
      }
      var placeActivites = function(response) {
        
        if(angular.isDefined(response.data.activities)) {
          for(var i = 0; i < response.data.activities.length ; i++) {
            $scope.activities.push(response.data.activities[i]);          
          }
        }
        if(angular.isDefined(response.data.bookTos)) {
          for(var i = 0; i < response.data.bookTos.length ; i++) {
            $scope.bookTos.push(response.data.bookTos[i]);        
            if(response.data.bookTos[i] == "Labour/Hr") {
              $scope.tscell.bookTo = $response.data.bookTos[i];
            }  
          }
        }
        
      }

      $scope.saveTime = function() {
        $scope.addTime(finishSaveTime);  

      }
      $scope.saveAndAdd = function() {
        $scope.addTime(saveTimeAndAdd);

      }

      $scope.addTime = function(callback) {
        var delDate = moment($scope.tscell.dateJS).format('DD/MM/YYYY');

        var data = {  date:delDate,
                      workid:$scope.tscell.work,
                      personid:$localStorage.personid, 
                      token:$localStorage.token, 
                      client:SubdomainService.company, 
                      remotepath:ConfigOptions.remotepath, 
                      userid: $localStorage.personid,
                      activity:$scope.tscell.activity,
                      bookTo:$scope.tscell.bookTo.id,
                      assigneeRel:$scope.tscell.bookTo.assigneeRel,
                      hours:$scope.tscell.effort,
                      comment:$scope.tscell.comment
                    }
        console.log(data);
        dataService.sendData(data, '/timesheet/add', callback); 

      }

      var finishSaveTime = function(response) {
        if(angular.isDefined(response.data.error)) {
          $mdToast.show(
                  $mdToast.simple()
                  .textContent(response.data.error)
                  .position("top" )
                  .hideDelay(3000)
                );

        } else {
          navService.navigateTo('#!/timesheet');
        }
      }
      var saveTimeAndAdd = function(response) {
        if(angular.isDefined(response.data.error)) {
          $mdToast.show(
                  $mdToast.simple()
                  .textContent(response.data.error)
                  .position("top" )
                  .hideDelay(3000)
                );

        } else {
          $mdToast.show(
                  $mdToast.simple()
                  .textContent("Cell Saved")
                  .position("top" )
                  .hideDelay(3000)
                );
          $scope.needEdit = true;
          
        }
      }
      $scope.selectActivity = function() {
        $scope.updateUnits();
      }
      $scope.updateUnits = function() {
        $scope.needEdit = false;
      }
      $scope.deleteCell = function(cell) {
        console.log(cell);
        var data = {  date:cell.date,
                      workid:cell.workid,
                      personid:$localStorage.personid, 
                      token:$localStorage.token, 
                      client:SubdomainService.company, 
                      remotepath:ConfigOptions.remotepath, 
                      userid: $localStorage.personid,
                      activity:cell.activityid,
                      bookTo:cell.bookTo,
                      assigneeRel:cell.assigneeRel
                    }
        dataService.sendData(data, '/timesheet/cell/delete', finishDeleteCell);                     
      }
      var finishDeleteCell = function(response) {
        console.log(response);
        if (response.data.message == "Timesheet Cell Deleted") {
          navService.navigateTo('#!/timesheet');
        }
      }
      $scope.duplicateCell = function(cell) {

      }
      $scope.selectTSCell = function(cell) {
        console.log(cell);
        $localStorage.selectedCell = cell;
        navService.navigateTo('#!/timesheet/edit');
      }
      $scope.initEdit = function() {
        $scope.tscell = $localStorage.selectedCell;
        $scope.user = $localStorage.user;
        $scope.savebutton = 'Save';
        $scope.tscell.editEffort = $scope.tscell.effort / 3600;
        $scope.tscell.dateJS = new Date(moment($scope.tscell.date).format('DD/MM/YYYY'));
        console.log($scope.tscell);
        $scope.needEdit = false;
      }

      $scope.greaterThan = function(prop, val){
          return function(item){
            return item[prop] > val;
          }
      }

      $scope.saveEdit = function() {
        console.log($scope.tscell);

        var data = {  date:$scope.tscell.date,
                      workid:$scope.tscell.workid,
                      personid:$localStorage.personid, 
                      token:$localStorage.token, 
                      client:SubdomainService.company, 
                      remotepath:ConfigOptions.remotepath, 
                      userid: $localStorage.personid,
                      activity:$scope.tscell.activityid,
                      bookTo:$scope.tscell.bookTo,
                      hours:$scope.tscell.editEffort,
                      comment:$scope.tscell.comment,
                      assigneeRel:$scope.tscell.assigneeRel
                    }
        
        var index = -1;

        for(var i = 0; i < $localStorage.timesheet.tscells.length; i++) {
          if($localStorage.timesheet.tscells[i].cellID == $scope.tscell.cellID) {
            index = i;
          }
        }
        if(index >= 0) {
          console.log(index);
          $localStorage.timesheet.tscells[index].effort = $scope.tscell.editEffort * 3600;
        }

        dataService.sendData(data, '/timesheet/cell/edit', finishSaveTime); 
      }
      $scope.groupingChange = function(state) {
        $scope.grouping = state;
        if(state) {
          $scope.groupingName = 'Date';
          $scope.groupingFilter = 'order';
        } else {
          $scope.groupingName = 'Project';
          $scope.groupingFilter = 'project';
        }
      }
      $scope.initAdd = function() {
        $scope.activities = [];
        $scope.bookTos = [];
        setDate();
        $scope.user = $localStorage.user;
        $scope.savebutton = 'Save & Back';
        $scope.work = $localStorage.user.hasWork;
        console.log($scope.work);

        $localStorage.showAddTime = false;
        $localStorage.showAddExp = false;
        $scope.needEdit = false;
      }

      $scope.init = function () {
        setDate();
        $scope.pageTitle = 'Timesheet';        
        $localStorage.pageTitle = $scope.pageTitle;
        $scope.name = $localStorage.name;
        $localStorage.showAddTime = true;
        $localStorage.showAddExp = false;
        $scope.grouping = true;
        $scope.groupingName = 'Date';
        $scope.groupingFilter = 'order';

        if(angular.isDefined($localStorage.timesheet)) {
          $scope.timesheet = $localStorage.timesheet;
        } else {
          $scope.isLoading = true;
        }

         DisplayData();

      };

      var logResponse = function(response) {
        console.log(response);
      }

      var setDate = function() {
        var currentDate = moment().startOf('isoweek').format('YYYYMMDD');
        if(SubdomainService.company == 'archdemo') {
          updateDates('20101004');
        } else {
          updateDates(currentDate);
        }
      }
      
      
}]);