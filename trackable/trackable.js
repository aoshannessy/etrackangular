'use strict';

angular.module('myApp.trackable', ['ngRoute'])

.controller('trackableController', ['$scope', '$http', '$localStorage', '$window', 'ConfigOptions', '$route', '$mdDialog', 'SubdomainService',  function($scope, $http, $localStorage, $window, ConfigOptions, $route, $mdDialog, SubdomainService) {


$scope.loadProgress = function() {  
      console.log('Loading Progress notes');
      
      var data = { itemTEID: $localStorage.teid,personid:$localStorage.personid, token:$localStorage.token, client:SubdomainService.company, remotepath:ConfigOptions.remotepath };
      console.log(data);
      sendData(data, "/contact/display/progress", finishProgress);
}
var finishProgress = function(response) {
    if(!angular.isDefined($scope.contact)) {
      $scope.contact = {};
    }
    $scope.contact.progress = response.data.progress;
    $scope.isLoading = false;
}

$scope.showDialog = function(saveFunc) {
    $mdDialog.show({
                  clickOutsideToClose: true,
                  scope: $scope,        
                  preserveScope: true,     
                  templateUrl: $scope.selectedTemplate,      
                  controller: function DialogController($scope, $mdDialog, $route) {
                     $scope.closeDialog = function() {
                        $mdDialog.hide();
                     }
                     $scope.saveDialog = function() {                      
                      saveFunc();                      
                      $scope.closeDialog();
                     }
                  }
               });
  }  

$scope.addProgress = function() {
  $scope.selectedTemplate = 'contact/dialogs/progressnote.html';
  $scope.showDialog(saveProgressNote);

}


var saveProgressNote = function() {
  console.log($scope.progressnote.text);
  var newNote = {
    date:moment().format('DD/MM/YYYY h:mm:ss A'),
    person:$localStorage.fullName,
    comment:$scope.progressnote.text
  }
  $scope.contact.progress.push(newNote);

  var data = {  itemTEID: $localStorage.teid,
                personid:$localStorage.personid, 
                token:$localStorage.token, 
                client:SubdomainService.company, 
                remotepath:ConfigOptions.remotepath, 
                note:$scope.progressnote.text };
  sendData(data, "/contact/progress/add", logResponse);
  $scope.progressnote.text = '';
}

var sendData = function(data, url, callback) {
  $http({
            method: 'POST',
            url:ConfigOptions.hostname +":" + ConfigOptions.goport + url, 
            params : data
            })
        .then(function(response) {  
            callback(response);            
      });

}

var logResponse = function(response) {
  console.log(response);
}

}]);
