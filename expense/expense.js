'use strict';

angular.module('myApp.expense', ['ngRoute','ngMaterial','ngMessages'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/expense', {
    templateUrl: 'expense/expense.html',
    controller: 'ExpenseCtrl'
  });
  $routeProvider.when('/expense/view', {
    templateUrl: 'expense/view.html',
    controller: 'ExpenseCtrl'
  });
  $routeProvider.when('/expense/create', {
    templateUrl: 'expense/create.html',
    controller: 'ExpenseCtrl'
  });  
  $routeProvider.when('/expense/user', {
    templateUrl: 'expense/user.html',
    controller: 'ExpenseCtrl'
  });  
  $routeProvider.when('/expense/edit', {
    templateUrl: 'expense/create.html',
    controller: 'ExpenseCtrl'
  });
  $routeProvider.when('/project', {
    templateUrl: 'expense/project.html',
    controller: 'ExpenseCtrl'
  });   
}])
.directive('fileInput', ['$parse',  function($parse){
  return {
    restrict: 'A',
    link:function(scope, element, attrs){
      element.bind('change', function(){
        $parse(attrs.fileInput)
        .assign(scope, element[0].files)
        $scope.$apply();
      })
    }
  }
}])
.controller('ExpenseCtrl', ['$scope', '$http', '$localStorage', '$window', 'userValidation', 'ConfigOptions', '$mdBottomSheet', '$mdToast', 'SubdomainService', '$route', 'navService', 'dataService', function($scope, $http, $localStorage, $window, userValidation, ConfigOptions, $mdBottomSheet, $mdToast, SubdomainService, $route, navService, dataService, fileInput) {
	   $scope.user = $localStorage.user;
     
     var DisplayData = function() {
      if(window.location.href == "http://localhost:8000/#!/expense/view") {
        console.log("Expense");
      } else {
        displayList();
        console.log(window.location.href);
      }
        
    }
    $scope.showExpense = function(expenseid) {
      // $localStorage.previousTitle = $scope.workName;
      // $localStorage.previous = '#!/expense';
      $localStorage.expenseid = expenseid;
      // $window.location.href = '#!/expense/view';
      navService.navigateTo('#!/expense/view');

    }
    $scope.showUserExpense = function(expenseid) {
      //$$localStorage.previousTitle = 'My Expenses';
      //$localStorage.previous = '#!/expense/user';
      $scope.pageTitle = 'My Expenses';
      $localStorage.pageTitle = $scope.pageTitle;
      $localStorage.expenseid = expenseid;
      navService.navigateTo('#!/expense/view');
      //$window.location.href = '#!/expense/view';

    }
    $scope.newExpense = function() {
      $localStorage.expenseid = -1;
      //$window.location.href = '#!/expense/create';  
      navService.navigateTo('#!/expense/create');    
    }
    $scope.selectCat = function() {
      for(var i = 0; i < $scope.categories.length; i++) {
        if($scope.categories[i].field == $scope.expense.expCat) {
          $scope.expense.unitcost = parseFloat($scope.categories[i].unitcost);
          $scope.expense.unitcharge = parseFloat($scope.categories[i].unitcharge);
        }
      }
      
    }
    $scope.updateUnits = function() {

      $scope.expense.actualcharge = $scope.expense.actualunits * $scope.expense.unitcharge;
      $scope.expense.actualcharge = parseFloat($scope.expense.actualcharge.toFixed(2));
      $scope.expense.actualcost = $scope.expense.actualunits * $scope.expense.unitcost;
      $scope.expense.actualcost = parseFloat($scope.expense.actualcost.toFixed(2));
    }
    $scope.createExpense = function() {
      $scope.isSaving = true;
      $scope.savebutton = "Creating...";
      var expdate = moment($scope.expense.dateJS).format('DD/MM/YYYY');
      var data = {'image': $scope.expense.image, 
                    'token':$localStorage.token, 
                    'client':SubdomainService.company, 
                    'personid':$localStorage.personid,
                    'expenseid':$localStorage.expenseid,
                    'actualcost':$scope.expense.actualcost,
                    'actualcharge':$scope.expense.actualcharge,
                    'actualunits':$scope.expense.actualunits,
                    'unitcharge':$scope.expense.unitcharge,
                    'unitcost':$scope.expense.unitcost,
                    'workid':$localStorage.workid,
                    'category':$scope.expense.expCat,
                    'date':expdate,
                    'note':$scope.expense.note,
                    'gst':$scope.expense.gst,
                    'invoice':$scope.expense.invoice,
                    'export':$scope.expense.export,
                    'reimburse':$scope.expense.reimburse,
                    'paid':$scope.expense.paid,
                    'approved':$scope.expense.approved,
                    'costcentre':$scope.expense.selectedcostcentre,

                    
                  }
      
      if(angular.isDefined($localStorage.expenseid) && $localStorage.expenseid > 0) 
      { //editExpense
        $scope.isEdit = true;
        console.log('Edit');
        $http({
                method: 'POST',
                url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/expenses/edit", 
                params : data,                 
              })
          .then(function(response) {              
              console.log(response);                
              userValidation.isValid(response.data.token); 
              
              $window.location.href = '#!/expense/view';              
              $scope.isSaving = false;
            });       
      }
      else 
      { //CreateExpense
        $scope.isEdit = false;
        console.log('Create');
        $http({
                method: 'POST',
                url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/expenses/create", 
                //url:"http://localhost:8080/expenses/create", 
                params : data,                 
              })
          .then(function(response) {
              
              console.log(response);
                
              userValidation.isValid(response.data.token); 
              $localStorage.expenseid = response.data.expenseid; 
              //$window.location.reload();
              $window.location.href = '#!/expense/view';  
            
              $scope.isSaving = false;
            });  
      }
    
      
      
    }
    $scope.imageChanged = function(elm) {
      $scope.image = elm.files[0];
      $scope.showUpload = true;
      $scope.$apply();
    }
    $scope.uploadImage = function() {

      $scope.showUpload = false;
      $scope.hideLoad = true;
      $scope.isLoading = true;
      $scope.imgSaving = true;

      var fd = new FormData();
      console.log($scope.image);
      console.log('Uploading...');
      fd.append('image', $scope.image);
      fd.append('token', $localStorage.token);
      fd.append('personid',$localStorage.personid);
      fd.append('expenseid',$localStorage.expenseid);
      fd.append('client', SubdomainService.company);
      fd.append('remotepath', SubdomainService.company);
      fd.append('work', $localStorage.workFull);
      console.log($localStorage.workFull);

      $http({
              method: 'POST',
              url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/expenses/uploadimage", 
              data: fd,
              headers: {'Content-Type': undefined},
                        transformRequest: angular.identity                  
            })
        .then(function(response) {
            
            console.log(response);
            $scope.expense.image = $scope.image;
              
            userValidation.isValid(response.data.token);  
            $window.location.reload();
            $route.reload();
          
            $scope.isLoading = false;
          });  
    }
    $scope.showActions = function() {
      $scope.alert = '';
      $mdBottomSheet.show({
        templateUrl: 'expense/expense_actions.html',
        controller: 'ExpenseCtrl'
      });
    };
    $scope.delete = function() {
      var data = {expenseid: $localStorage.expenseid, token:$localStorage.token, personid: $localStorage.personid, client:SubdomainService.company};
      $http({
              method: 'POST',
              url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/expenses/delete", 
              params : data
            })
        .then(function(response) {
            userValidation.isValid(response.data.token); 
            $mdBottomSheet.hide();           
            $window.location.href = '#!/expense';
          });


    }
    $scope.edit = function() {
      $mdBottomSheet.hide();

      $window.location.href = '#!/expense/edit';      
    }
    $scope.backToList = function() {
      $window.location.href = '#!/expense';
      $localStorage.expenseid = -1;
    }

    var displayList = function() {
        $scope.isLoading = true;
        $scope.workName = $localStorage.workFull;
        $scope.parent = $localStorage.parent; 
        
        var data = {personid: $localStorage.personid, workid:$localStorage.workid, token:$localStorage.token, date:"10/10/2000", datefinish:"10/10/2018", client:SubdomainService.company};
        $http({
              method: 'POST',
              url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/expenses/display", 
              params : data
            })
        .then(function(response) {           
            $scope.expenses = response.data.expenses;   
            $scope.message = response.data.message;  
            if(angular.isDefined($scope.message)) {
              $scope.messageExists = true;
            }         
            userValidation.isValid(response.data.token);          
            $scope.isLoading = false;
          });
    }
    var displayMyExpenses = function() {
      $scope.isLoading = true;
      $localStorage.showAddExp = true;
      clearExpenseStorage();
      
      displayDateRange();

      loadMyExpenses();
    }
    var loadMyExpenses = function() {
      var data = {personid: $localStorage.personid, workid:$localStorage.workid, token:$localStorage.token, date:$scope.delphiStartDate, datefinish:$scope.delphiEndDate, client:SubdomainService.company};
      $http({
          method: 'POST',
          url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/expenses/user", 
          params : data
      })
      .then(function(response) {           
        $scope.expenses = response.data.expenses;  
        if(angular.isDefined($scope.expenses)){  
          for(var i = 0; i < $scope.expenses.length; i++) {
            $scope.expenses[i].sortDate =  moment(response.data.expenses[i].date, "DD/MM/YYYY").unix();
          }
        }    
        $scope.message = response.data.message;  
        if(angular.isDefined($scope.message)) {
          $scope.messageExists = true;
        }   
        userValidation.isValid(response.data.token);          
        $scope.isLoading = false;
      });
    }
    var displayExpense = function() {
      userValidation.isValid($localStorage.token);
      $scope.showUpload = false;
      $scope.catNotSelected = true;
      $scope.catSelected = false;
      $scope.hideLoad = true;
      
      var data = {expenseid: $localStorage.expenseid, token:$localStorage.token, personid: $localStorage.personid, client:SubdomainService.company};
        $http({
              method: 'POST',
              url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/expenses/view", 
              params : data
                  //headers: 'Content-Type': 'application/x-www-form-urlencoded'
                      
                  
            })
        .then(function(response) {
            userValidation.isValid(response.data.token);
            console.log(response.data);
            $scope.expense = response.data;
            $scope.workName = $localStorage.workFull;

            $scope.expense.actualunits = parseFloat($scope.expense.actualunits);
            $scope.expense.actualcost = parseFloat($scope.expense.actualcost);
            $scope.expense.actualcharge = parseFloat($scope.expense.actualcharge);            
            $scope.expense.unitcharge = parseFloat($scope.expense.unitcharge);
            $scope.expense.unitcost = parseFloat($scope.expense.unitcost);
            var date = moment($scope.expense.date, 'DD/MM/YYYY');
            $scope.expense.dateJS = date.toDate();
            $scope.expense.expCat = $scope.expense.category;
            var path = SubdomainService.company+'/app/Receipts/';
            $scope.expense.image = path+response.data.image;

            if(response.data.image == '' || response.data.image == path ) {
              $scope.hideImage = true;
            } else {
              var imgdata = {token:$localStorage.token, personid: $localStorage.personid, client:SubdomainService.company, imagepath:$scope.expense.image}; 
              dataService.sendData( imgdata, "/image/retrieve", finishImgRetrieve);
              $scope.imagedata = true;              
              $scope.hideImage = true;
            }


            $scope.expense.gst = (response.data.gst == 'true');
            $scope.expense.reimburse = (response.data.reimburse == 'true');            
            $scope.expense.approved = (response.data.approved == 'true');
            $scope.expense.paid = (response.data.paid == 'true');
            $scope.expense.invoice = (response.data.invoice == 'true');
            $scope.expense.export = (response.data.export == 'true');
            $scope.expense.selectedcostcentre = $scope.expense.costcentre;



//            loadImage();
          });


        
    }
    var finishImgRetrieve = function(response) {
      console.log(response.data);
      $scope.expense.imagedata = response.data;
    }

    var displayCreate = function() {
      $scope.saveButton = "Create Expense";
      $scope.showUnits = false

      console.log($scope.user);
      $scope.workName = $localStorage.workFull;
      $scope.userName = $localStorage.fullName;
      var data = {token:$localStorage.token, personid: $localStorage.personid, client:SubdomainService.company};
        $http({
              method: 'POST',
              url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/expenses/new", 
              params : data
                  //headers: 'Content-Type': 'application/x-www-form-urlencoded'
                      
                  
            })
        .then(function(response) {          
            userValidation.isValid(response.data.token);
            console.log(response.data);
            console.log(response.data.categories);
            $scope.categories = response.data.categories;
            $scope.costcentres = response.data.costcentres;
            $scope.savebutton = 'Create Expense';

            if($localStorage.expenseid > 0) {
              displayExpense();
              $scope.catNotSelected = false;
              $scope.catSelected = true;
              $scope.isEdit = true;
              $scope.savebutton = 'Save Expense';
              // $scope.workName = $localStorage.workFull; 
              // $scope.actualcost = $scope.expense.actualcost;
              // $scope.actualcharge = $scope.expense.actualcharge;
              // $scope.unitcharge = $scope.expense.unitcharge;
              // $scope.unitcost = $scope.expense.unitcost;
              // //$localStorage.workid = $scope.expense.w
              // $scope.expCat = $scope.expense.expCat;
              // //var expdate = moment($scope.date).format('DD/MM/YYYY');                  
              // $scope.date = $scope.expense.date;
              // $scope.note = $scope.expense.note;
              // $scope.gst = $scope.expense.gst;
              // $scope.invoice = $scope.expense.invoice;
              // $scope.export = $scope.expense.export;
              // $scope.reimburse = $scope.expense.reimburse;
              // $scope.paid = $scope.expense.paid;
              // $scope.approved = $scope.expense.approved;
              // $scope.selectedcostcentre = $scope.expense.costcentre;            
              

            } else {
              $scope.isEdit = false;

              $scope.expCat = '';
              $scope.expense = {};
              $scope.expense.dateJS = new Date(); 
              $scope.actualcharge = '';
              $scope.actualunits = '';
              $scope.actualcost = '';
              $scope.unitcost = '';
              $scope.unitcharge = '';
            }
            
                                
            
            $scope.isLoading = false;
          });

    }
    var displayDateRange = function() {
      var now = new Date();      
      var currentDateString = moment(now).startOf('month').format('MM/DD/YYYY');         
      var onemonthString = moment(now).endOf('month').format('MM/DD/YYYY'); 

      var currentDate = new Date(currentDateString);
      var oneMonth = new Date(onemonthString);

      $scope.dateStart = currentDate;// {value: new Date(currentDate)};
      $scope.dateEnd = oneMonth; //{value: new Date(onemonth)};

      $scope.delphiStartDate = moment(currentDate).format('DD/MM/YYYY');
      $scope.delphiEndDate = moment(oneMonth).format('DD/MM/YYYY');
    }
    $scope.updateDateRange = function() {
      $scope.delphiStartDate = moment($scope.dateStart).format('DD/MM/YYYY');
      $scope.delphiEndDate = moment($scope.dateEnd).format('DD/MM/YYYY');

      loadMyExpenses();
    }

    $scope.loadImage = function() {
      $scope.imgLoading = true;
      var data = {expenseid: $localStorage.expenseid, token:$localStorage.token, client:SubdomainService.company};
      $http({
              method: 'POST',
              url:ConfigOptions.hostname +":" + ConfigOptions.goport + "/expenses/view/image", 
              params : data
                  //headers: 'Content-Type': 'application/x-www-form-urlencoded'
                      
                  
            })
        .then(function(response) {
            console.log(response.data);
            userValidation.isValid(response.data.token);
            console.log(response.data);
            // var imgPath = response.data.image
            // imgPath = imgPath.replace('/', '\\');
            // console.log(imgPath);            
            if(response.data.image == '') {
              
              $mdToast.show(
                $mdToast.simple()
                  .textContent('No Image for this expense')
                  .position("top" )
                  .hideDelay(3000)
              );
            }
            $scope.expense.image = '/Receipts/'+response.data.image;
            $scope.imgLoading = false;
            
          });
    }


    

    var clearExpenseStorage = function () {
      $localStorage.workFull = '';
      $localStorage.fullName = '';
      $localStorage.expenseid = -1;

    }
    $scope.isUndefined = function (thing) {
      return (typeof thing === "undefined");
    }
    var navigateTo = function(destination) {
      $localStorage.previousTitles.push($scope.pageTitle);
      $localStorage.previousURLs.push($window.location.href);
      if(angular.isDefined($localStorage.teid)) {
        $localStorage.previousTEIDs.push($localStorage.teid);
      } else {
        $localStorage.previousTEIDs.push(-1);
      }
      console.log($localStorage.previousTitles);
      console.log($localStorage.previousURLs);

      $window.location.href = destination;
    }

    $scope.init = function () {
      $scope.pageTitle = $localStorage.workFull;
      $localStorage.pageTitle = $scope.pageTitle;
    	displayList();
    }	
    $scope.initView = function() {
      $localStorage.showAddTime = false;
      $localStorage.showAddExp = false;
      displayExpense();
    }
    $scope.initCreate = function() {
      $localStorage.showAddTime = false;
      $localStorage.showAddExp = false;
      displayCreate();
    }
    $scope.initMyExpense = function () {
      $localStorage.showAddTime = false;
      $localStorage.showAddExp = false;
      displayMyExpenses();
    }
    //init();


}]);