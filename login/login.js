'use strict';

angular.module('myApp.login', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'login/login.html',
    controller: 'LoginCtrl'
  });
}])

.controller('LoginCtrl', ['$scope', '$http', '$localStorage', '$window', 'ConfigOptions', '$mdToast', 'SubdomainService', function($scope, $http, $localStorage, $window, ConfigOptions, $mdToast, SubdomainService) {
	var port = 8080;
  $scope.initLogin = function() {
    $scope.isLoading = false;
    $scope.LoginMessage = "Login";
  }
	$scope.login = function() {
      $scope.isLoading = true;
      $scope.LoginMessage = "Logging In..."
    	var url = ConfigOptions.hostname +":" + ConfigOptions.goport + '/user/login';
      
      console.log(SubdomainService.company);
      	var data = {username: $scope.user.username ,password: $scope.user.password, client:SubdomainService.company};
      	console.log(url);
      	$http({
            method: 'POST',
            url:url,
            params : data
                //headers: 'Content-Type': 'application/x-www-form-urlencoded'
                    
                
        })
        .then(function(response) {
            if (response.data.personid > 0){
                $localStorage.user = response.data;
                $localStorage.personid = response.data.personid;
                $localStorage.first_name = response.data.first_name;
                $localStorage.surname = response.data.surname;
                $localStorage.fullName = response.data.first_name + ' ' + response.data.surname;
                $localStorage.initials = response.data.first_name.charAt(0) + response.data.surname.charAt(0);
                $localStorage.user.fullName = $localStorage.fullName;

                console.log($localStorage.user.flags);

                $localStorage.token = response.data.token;
                $localStorage.previousTitles = [];
                $localStorage.previousURLs = [];
                $localStorage.previousTEIDs = [];
                $localStorage.showAddExp = false;
                $localStorage.showAddTime = false;
                console.log($localStorage);

              	$window.location.href = '#!/home';
              	
            }
            if(angular.isDefined(response.data.message)) { 
              $scope.isLoading = false;
              $scope.LoginMessage = "Login";
                $mdToast.show(
                  $mdToast.simple()
                  .textContent(response.data.message)
                  .position("top" )
                  .hideDelay(3000)
                );
            }
        }, function errorCallback(response) {
        	console.log(response);
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		});

    }

}]);